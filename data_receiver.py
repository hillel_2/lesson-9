import json

import requests

ppl = []
for i in range(10):
    data = requests.get('https://randomuser.me/api/')
    random_user = data.json()
    ppl.extend(random_user.get('results'))
with open('ppl.json', 'w') as f:
    json.dump(ppl, f, indent=4)
print(f"Hello, my name is {ppl[0].get('name', {}).get('first').title()}")


data = requests.get('https://nekos.best/api/v2/highfive')
random_image_data = data.json()
results = random_image_data.get('results')

image = requests.get(results[0].get('url'))
with open(results[0].get('anime_name') + '.gif', 'wb') as f:
    f.write(image.content)

#data = requests.get(f'https://www.boredapi.com/api/activity?type={params["type"]}&participants={params["participants"]}')
params = {'type': 'cooking', 'participants': 2}
data = requests.get('https://www.boredapi.com/api/activity', params=params)
print(data.json())