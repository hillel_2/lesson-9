class UserModel:
    def __init__(self, id, username, first_name, last_name, country, email, dob):
        self.id = id
        self.username = username
        self.first_name = first_name
        self.last_name = last_name
        self.country = country
        self.email = email
        self.dob = dob
