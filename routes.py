import sqlite3

from flask import render_template, request, redirect

from app import app
from db import get_connection
from forms import LoginForm
from models import UserModel


@app.route('/')
def main():
    return render_template('index.html')

@app.route('/login', methods=['GET', 'POST'])
def login():
    method = request.method
    form = LoginForm()
    if method == 'POST':
        if form.validate():
            print(request.form['username'])
            print(request.form['password'])
            username = form.username.data
            with get_connection(app.config.get('DB_URL')) as conn:
                cursor = conn.cursor()
                user_data = cursor.execute('SELECT * FROM ppl WHERE username = ?', (username,))
                user = dict(user_data.fetchone())
                return redirect(f'/user/{user["id"]}')
        return redirect(f'/login')
    else:
        return render_template('login.html', form=form)

@app.route('/user/<id>')
def user(id):
    with get_connection(app.config.get('DB_URL')) as conn:
        cursor = conn.cursor()
        user_data = cursor.execute('SELECT id, username, first_name, last_name, country, email, dob FROM ppl WHERE id = ?', (id,))
        user = dict(user_data.fetchone())
        user_model = UserModel(**user)
        return render_template('user.html', user=user_model)