import json
import sqlite3


def get_connection(db_name: str):
    connection = sqlite3.connect(db_name)
    connection.row_factory = sqlite3.Row
    return connection


def init_db(db_name: str):
    with get_connection(db_name) as db:
        cursor = db.cursor()
        cursor.execute('CREATE TABLE ppl('
                       'id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,'
                       'username VARCHAR(40) NOT NULL,'
                       'first_name VARCHAR(40) NOT NULL,'
                       'last_name VARCHAR(40) NOT NULL,'
                       'country VARCHAR(40) NOT NULL,'
                       'email VARCHAR(40) NOT NULL,'
                       'dob VARCHAR(40) NOT NULL'
                       ');')
        with open('ppl.json') as f:
            ppl = json.load(f)
        for person in ppl:
            cursor.execute('INSERT INTO ppl(username, first_name, last_name, country, email, dob) VALUES (?, ?, ?, ?, ?, ?)',
                           (
                            person['login']['username'],
                            person['name']['first'],
                            person['name']['last'],
                            person['location']['country'],
                            person['email'],
                            person['dob']['date'],
                            ))
        cursor.close()
        db.commit()
